<?php
session_start();

if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != ''){ ?>

<?php include_once ('header.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-md-6 mt-5">  
            <div class="card card-success">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-6">
                    <h2 class="card-title">Add Manufacturer</h2>
                  </div>
                  <div class="col-md-6">
                    <a href="view_manufacturer.php" class="nav-link active float-right">
                      <i class="nav-icon fa-2x fa fa-list"></i>
                    </a>
                </div>
              </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label>Manufacturer's name</label>
                    <input type="text" class="form-control" id="manufacturer" placeholder="manufacturer's name" name="manufacturer">
                    <p id="manufacturer_comment"></p>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" id="man_btn">Add Manufacturer</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card -->
    </section>
  </div>
  <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
  </div>
<?php include_once('footer.php'); ?>

<?php }else{
  echo "Login to access this page.";
}?>