<?php
session_start();

if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != ''){ ?>

<?php include_once ('header.php'); ?>
<?php include_once ('model/manufacturer.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-md-6 mt-5">  
            <div class="card card-info">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-6">
                    <h2 class="card-title">Add New Model</h2>
                  </div>
              </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="insert_model.php" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Model's name</label>
                        <input type="text" class="form-control"  placeholder="Model's name" name="mod_name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Manufacturer</label>
                          <select class="form-control" name="man_id">
                              <?php while($res =  mysqli_fetch_assoc($query) ) { ?>
                                <option value="<?php echo $res['id']; ?>"><?php echo $res['name']; ?></option>
                              <?php } ?>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Count</label>
                        <input type="number" class="form-control"  placeholder="Count" name="count" min="0">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="exampleFormControlFile1">Image</label>
                          <input type="file" class="form-control-file" name="file_to_upload">
                      </div>
                    </div>

                  </div>
                  <div>
                    <button type="submit" class="btn btn-primary">Add  Model</button>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card -->
    </section>
  </div>
  <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
  </div>
<?php include_once('footer.php'); ?>

<?php }else{
  echo "Login to access this page.";
}?>