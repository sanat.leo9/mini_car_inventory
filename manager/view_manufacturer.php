<?php
session_start();

if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != ''){ ?>

<?php include_once ('header.php'); ?>
<?php include_once ('model/manufacturer.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <section class="content">
      <div class="container-fluid">
        <div class="row justify-content-md-center">
          <div class="col-md-8 mt-5">  
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Manufacturers</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Manufacturer's Name</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $i = 0; ?>
                  <?php while( $res = mysqli_fetch_assoc($query) ) { ?>
                    <tr>
                      <td><?php echo ++$i; ?></td>
                      <td><?php echo $res['name'] ?></td>
                      <td>
                        <button class="btn btn-success" data-toggle="modal" data-target="#editModal<?php echo $res['id']; ?>">
                        Edit
                        </button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal<?php echo $res['id'];?>">Delete</a>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>

        <!-- Modal -->
        <?php while( $res = mysqli_fetch_assoc($query_modal) ) { ?>
          <div class="modal fade" id="editModal<?php echo $res['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Edit Manufacturer</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                  <form role="form" action="update_manufacturer.php" method="post">
                    <div class="card-body">
                      <div class="form-group">
                        <label>MAnufacturer's name</label>
                        <input type="text" class="form-control" value="<?php echo $res['name']; ?>" name="name">
                        <input type="hidden" name="id" value="<?php echo $res['id']; ?>">
                      </div>
                    </div>
                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary" >Edit Manufacturer</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>

        <!-- Modal -->
        <?php while( $res = mysqli_fetch_assoc($delete_modal) ) { ?>
          <div class="modal fade" id="deleteModal<?php echo $res['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Delete Manufacturer</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                  <h2 class="modal-title" id="myModalLabel">Are You Sure</h2>
                    <div class="card-footer">
                      <a href="delete_manufacturer.php?id=<?php echo $res['id']; ?>" class="btn btn-danger" >Yes</a>
                      <button type="button" class="btn btn-primary " data-dismiss="modal" aria-label="Close">No
                      </button>
                    </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <!-- /.card -->
    </section>
  </div>
  <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
  </div>
<?php include_once('footer.php'); ?>

<?php }else{
  echo "Login to access this page.";
}?>