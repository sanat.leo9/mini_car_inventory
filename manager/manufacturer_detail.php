<?php
session_start();

if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != ''){ ?>

<?php include_once ('header.php'); ?>
<?php include_once ('model/manufacturer_detail.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <section class="content">
      <div class="container-fluid">
        <div class="row justify-content-md-center">
          <div class="col-md-12 mt-5">  
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Models</h3>
                <input type="hidden" id="man_id" value="<?php echo $id; ?>">
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Model's Name</th>
                    <th>Model Count</th>
                    <th>Model's Image</th>
                    <th>Sold</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $i = 0; ?>
                  <?php while( $res = mysqli_fetch_assoc($query) ) { ?>
                    <tr>
                      <td><?php echo ++$i; ?></td>
                      <td><?php echo $res['mod_name']; ?></td>
                      <td><?php echo $res['count'];?></td>
                      <td><img src="uploads/<?php echo $res['file_to_upload'];?>" width=200></td>

                      <td>
                        <a href="add.php?id=<?php echo $res['id']; ?>&&man_id=<?php echo $res['man_id']; ?>" class="btn btn-info" >
                         <i class="fa fa-2x fa-plus-circle" aria-hidden="true"></i>
                        </a>
                        <a href="sold.php?id=<?php echo $res['id']; ?>&&man_id=<?php echo $res['man_id']; ?>" class="btn btn-primary" >
                         <i class="fa fa-2x fa-minus-circle" aria-hidden="true"></i>
                        </a>
                      </td>

                      <td>
                        <a href="edit_manufacturer_detail.php?id=<?php echo $res['id']; ?>&&man_id=<?php echo $res['man_id']; ?>" class="btn btn-success" >
                        Edit
                        </a>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal<?php echo $res['id'];?>">
                          Delete
                        </button>
                      </td>
                    </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <?php while( $res = mysqli_fetch_assoc($delete_modal) ) { ?>
        <div class="modal fade" id="deleteModal<?php echo $res['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Delete Model</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <h2 class="modal-title" id="myModalLabel">Are You Sure</h2>
                  <div class="card-footer">
                    <a href="delete_model.php?id=<?php echo $res['id']; ?>&&man_id=<?php echo $id; ?>" class="btn btn-danger" >Yes</a>
                    <button type="button" class="btn btn-primary " data-dismiss="modal" aria-label="Close">No
                    </button>
                  </div>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <!-- /.card -->
    </section>
  </div>
  <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
  </div>
<?php include_once('footer.php'); ?>

<?php }else{
  echo "Login to access this page.";
}?>