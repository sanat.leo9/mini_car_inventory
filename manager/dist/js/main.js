 $(document).ready(function(){
    $("#man_btn").click(function(e){
        e.preventDefault();
        var manufacturer = $('#manufacturer').val();
        	if (manufacturer == "") {
                $('#manufacturer_comment').fadeIn();
        		$('#manufacturer_comment').text("This field cannot be empty");
        		$('#manufacturer_comment').css("color","red");
        		$('#manufacturer_comment').delay('3000').fadeOut();
        	}else{
        		$.ajax({
        		       url: "insert_manufacturer.php",
        		       type: "post",
        		       data: {'name' : manufacturer},
        		       success: function (data) {
                          $('#manufacturer_comment').fadeIn();
        		          $('#manufacturer_comment').text("manufacturer added successfully");
        		          $('#manufacturer_comment').css("color","#5cb85c");
        		          $('#manufacturer_comment').delay('3000').fadeOut();
        		          $('#manufacturer').val('');
        		       }
        		   });
        	}
    });
});
