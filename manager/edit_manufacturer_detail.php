<?php
session_start();

if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != ''){ ?>

<?php include_once ('header.php'); ?>
<?php include_once ('model/manufacturer.php'); ?>
<?php 
$id = $_GET['id'];
$man_id = $_GET['man_id'];
$select_edit = "SELECT * FROM models WHERE `id`=".$id." AND `man_id`=".$man_id;
$query_edit = mysqli_query($conn, $select_edit);
$res_edit = mysqli_fetch_assoc($query_edit);
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <section class="content">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col-md-6 mt-5">  
            <div class="card card-info">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-6">
                    <h2 class="card-title">Add New Model</h2>
                  </div>
              </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="update_model.php" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Model's name</label>
                        <input type="text" class="form-control"  value="<?php echo $res_edit['mod_name']; ?>" name="mod_name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label>Manufacturer</label>
                          <select class="form-control" name="man_id">
                              <?php while($res =  mysqli_fetch_assoc($query) ) { ?>
                                <option value="<?php echo $res['id']; ?>" <?php if( $res['id'] == $res_edit['id'] ){ echo "selected"; } ?>>
                                  <?php echo $res['name']; ?>
                                  </option>
                              <?php } ?>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Count</label>
                        <input type="number" class="form-control"  value="<?php echo $res_edit['count']; ?>" name="count" min="0">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="exampleFormControlFile1">Image</label>
                          <input type="file" class="form-control-file" name="file_to_upload">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="exampleFormControlFile1">Current Image</label>
                          <img src="uploads/<?php echo $res_edit['file_to_upload']; ?>">
                      </div>
                    </div>

                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <input type="hidden" name="old_img" value="<?php echo $res_edit['file_to_upload']; ?>">
                  </div>
                  <div>
                    <button type="submit" class="btn btn-success">Edit Model</button>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card -->
    </section>
  </div>
  <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
  </div>
<?php include_once('footer.php'); ?>

<?php }else{
  echo "Login to access this page.";
}?>