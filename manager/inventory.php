<?php
session_start();

if(isset($_SESSION['user_id']) && $_SESSION['user_id'] != ''){ ?>

<?php include_once ('header.php'); ?>
<?php include_once ('model/inventory.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <section class="content">
      <div class="container-fluid">
        <div class="row justify-content-md-center">
          <div class="col-md-12 mt-5">  
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Inventory</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <?php while( $res = mysqli_fetch_assoc($query) ) { ?>
                    <div class="col-md-4">
                      <div class="well">
                       Manufacturer : 
                        <a href="manufacturer_detail.php?id=<?php echo $res['id']; ?>">
                          <?php echo $res['name']; ?>
                        </a>
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div>
      <!-- /.card -->
    </section>
  </div>
  <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
  </div>
<?php include_once('footer.php'); ?>

<?php }else{
  echo "Login to access this page.";
}?>